//Create a new instance of the House constructor, calling it myHouse and passing a number of bedrooms. 
//Then, use instanceof to verify that it is an instance of House.

// Assignment OH 25 November - Heru Benowo
function House(numBedrooms) {
    this.numBedrooms = numBedrooms;
    console.log(`In my house, there are ${numBedrooms} bedrooms`)
}
  
// Only change code below this line

let myHouse = new House(3);
console.log(myHouse instanceof House);
/*
myHouse should have a numBedrooms attribute set to a number.

You should verify that myHouse is an instance of House using the instanceof operator.
*/