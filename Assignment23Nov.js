//Declare and initialize a variable total to 0. Use a for loop to add the value of each element of the myArr array to total.


var myArr = [ 2, 3, 4, 5, 6];
var total = 0
// With For Loop
for (let i = 0; i < myArr.length; i++) {
    total += myArr[i]
}
// Without For Loop
let sum = myArr.reduce((x,y ) => x + y) // reduce = mereduce elemen array menjadi single value dengan menjumlah setiap elemen dari kiri ke kanan

console.log(`Jumlah elemen dalam array: ${myArr.length}\n`);
console.log(`With for loop`);
console.log(`=============`);
console.log(`Total penjumlahan: ${total}\n`);
console.log(`Without for loop`);
console.log(`================`);
console.log(`Total penjumlahan: ${sum}`);
//total should equal 20.