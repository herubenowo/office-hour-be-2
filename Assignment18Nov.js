/* CHECKING TEST SCORE TASK */
/* Write if-else statements to fulfill the following conditions:
if score morethan 60 you pass otherwise you failed */

function testScore(yourscore) {
  // code below this line
    if (yourscore > 60) {
        return (`Passed`);
    } else {
        return (`Failed`);
    }
}

console.log(testScore(50));
console.log(testScore(90));

// testScore(50) should return "Failed"
// testScore(90) should return "Passed"