/* Write a switch statement which tests val and sets answer for the following conditions:
1 - "alpha"
2 - "beta"
3 - "gamma"
4 - "delta"
*/

/* HERU BENOWO */

function caseInSwitch(val) {
    switch (Number(val)) {
        case 1:
            return (`alpha`);
            break;
        case 2:
            return (`beta`);
            break;
        case 3:
            return (`gamma`);
            break;
        case 4:
            return (`delta`);
            break;
        default:
           return (`Your input must be 1 - 4 !`);
    }
}
 
console.log(caseInSwitch("1"));
console.log(caseInSwitch(2));
console.log(caseInSwitch("3"));
console.log(caseInSwitch(4));
console.log(caseInSwitch(5));
 
//caseInSwitch(1) should have a value of "alpha"