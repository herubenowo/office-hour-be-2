/* Office Hour's Assignment Week2 Day3 */
/* Heru Benowo */

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
function multiply(num1, num2) {
    return num1 * num2;
}
function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}
function inputNum1() {
    rl.question(`1st Number: `, num1 => {
        if (!isNaN(num1) && !isEmptyOrSpaces(num1)) {
            inputNum2(num1)
        } else {
            console.log(`Your input must be a number bro!\n`);
            inputNum1()
        }
    })
}
function inputNum2(num1) {
    rl.question(`2nd Number: `, num2 => {
        if (!isNaN(num2) && !isEmptyOrSpaces(num2)) {
            console.log(`${num1} x ${num2} = ${multiply(num1, num2)}`);
            inputAgain();
        } else {
            console.log(`Your input must be a number bro!\n`);
            inputNum2(num1)
        }
    })
}
function inputAgain(answer) {
    rl.question(`Want to do another multiply, bro?(y/n) `, wish => {
        if (wish == `y`|| wish == `Y`) {
            inputNum1();
        } else if (wish == `n` || wish == `N`) {
            process.exit();
        } else {
            console.log(`Your input must be "y" or "n", bro!\n`);
            inputAgain();
        }
    })
}
console.clear()
console.log(`You can multiply your number here!`);
console.log(`==================================`);
console.log(`Ex: 1st Number x 2nd Number = Your Result\n`);
inputNum1();