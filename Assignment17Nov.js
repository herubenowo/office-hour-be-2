/* Checking Number Task */

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}

function checkNumber(input) {
    rl.question(`Input your number here: `, input => {
        if (!isNaN(input) && !isEmptyOrSpaces(input)) {
            console.log(`${input} is a number(TRUE)`);
            rl.close();
        } else {
            console.log(`${input} is not a number(FALSE)`);
            rl.close();
        }
    })
        
}

console.log("Check your input is a number or not");
console.log(`-----------------------------------`);
checkNumber();